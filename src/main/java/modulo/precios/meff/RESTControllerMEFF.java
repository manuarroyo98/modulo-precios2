package modulo.precios.meff;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import modulo.precios.meff.entities.EntityDatosAnuales;
import modulo.precios.meff.entities.EntityDatosDiarios;
import modulo.precios.meff.entities.EntityDatosMensuales;
import modulo.precios.meff.entities.EntityDatosTrimestrales;
import modulo.precios.meff.entities.EntityRegistroDiario;
import modulo.precios.meff.services.ServiceDatosAnuales;
import modulo.precios.meff.services.ServiceDatosDiarios;
import modulo.precios.meff.services.ServiceDatosMensuales;
import modulo.precios.meff.services.ServiceDatosTrimestrales;
import modulo.precios.meff.services.ServiceRegistroDiario;
import modulo.precios.omie.services.ServiceFechaLecturaOMIE;
import modulo.precios.omie.services.ServiceFestivoOMIE;

@RestController
@RequestMapping(RESTControllerMEFF.BASE_URL)
public class RESTControllerMEFF {

	// Url base del controller, todos los metodos se accederan como la BASE_URL +
		// ruta del metodo

		public static final String BASE_URL = "/moduloMEFF/api/v1/";

		
		private static final Logger rlogger = LoggerFactory.getLogger(RESTControllerMEFF.class);


		@Autowired
		private ServiceRegistroDiario registroDiarioService;
		@Autowired
		private ServiceDatosDiarios datosDiariosService;
		@Autowired
		private ServiceDatosMensuales datosMensualesService;
		@Autowired
		private ServiceDatosTrimestrales datosTrimestralesService;
		@Autowired
		private ServiceDatosAnuales datosAnualesService;
		
		

		public RESTControllerMEFF(ServiceRegistroDiario registroDiarioService, ServiceDatosDiarios datosDiariosService,
				ServiceDatosMensuales datosMensualesService, ServiceDatosTrimestrales datosTrimestralesService,
				ServiceDatosAnuales datosAnualesService) {
			super();
			this.registroDiarioService = registroDiarioService;
			this.datosDiariosService = datosDiariosService;
			this.datosMensualesService = datosMensualesService;
			this.datosTrimestralesService = datosTrimestralesService;
			this.datosAnualesService = datosAnualesService;
		}

		@CrossOrigin
		@GetMapping(path = "precioCierre", produces = "application/json")
		@ResponseStatus(HttpStatus.CREATED)
		public EntityRegistroDiario getRegistroDiario(HttpServletRequest request,
				@RequestParam(required = false) String strFecha) {
			Date fecha;
			EntityRegistroDiario registroDiario = new EntityRegistroDiario();
			rlogger.info("Busco precioCierre por fecha");
			try {
				// Convierto la fecha de formato String a formato Date si el usuario no ha pasado una fecha le damos la actual
				if (strFecha != null) {
					fecha = new SimpleDateFormat("yyyy-MM-dd").parse(strFecha);
				} else {
					fecha = new Date();
				}
				// Realizo la consulta y devuelvo el objeto
				registroDiario = registroDiarioService.buscarPorFecha(fecha);
				return registroDiario;
			} catch (ParseException e) {
				// Si la fecha no tiene un formato correcto, pinto el error y devuelvo un bad
				// request
				rlogger.error("Error al formatear la fecha. \n\tFecha URL: " + strFecha);
				e.printStackTrace();
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Formato de fecha es incorrecto", e);
			} catch (NullPointerException e) {
				return new EntityRegistroDiario();
			} catch (Exception e) {
				rlogger.error(e.getMessage());
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error desconocido", e);
			}
		}

		@CrossOrigin
		@GetMapping(path = "rangoPrecioCierre", produces = "application/json")
		@ResponseStatus(HttpStatus.CREATED)
		public List<EntityRegistroDiario> getRegistroDiarioRango(HttpServletRequest request, @RequestParam String strFechaInicio,
				@RequestParam String strFechaFin) {
			DateTime fechaInicio, fechaFin;
			List<EntityRegistroDiario> listaRegistroDiario = new ArrayList<EntityRegistroDiario>();
			rlogger.info("Busco precioCierre por rango fecha");
			try {
				// Convierto las fechas de formato String a formato Date
				fechaInicio = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(strFechaInicio);
				fechaFin = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(strFechaFin).plusDays(1);

				// Realizo la consulta y devuelvo el objeto
				listaRegistroDiario = registroDiarioService.buscarEntreFechas(fechaInicio.toDate(), fechaFin.toDate());
				return listaRegistroDiario;
			} catch (IllegalFieldValueException e) {
				// Si la fecha no tiene un formato correcto, pinto el error y devuelvo un bad
				// request
				rlogger.error("Error al formatear las fechas. \n\tFecha Inicio: " + strFechaInicio + " \n\tFecha Fin: "
						+ strFechaFin);
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Formato de fecha es incorrecto", e);
			} catch (NullPointerException e) {
				return new ArrayList<EntityRegistroDiario>();
			} catch (Exception e) {
				rlogger.error(e.getMessage());
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error desconocido", e);
			}
		}

		@CrossOrigin
		@GetMapping(path = "precioCierre/porDia", produces = "application/json")
		@ResponseStatus(HttpStatus.CREATED)
		public ObjDatosGenerales getDatosGenerales(HttpServletRequest request, @RequestParam String strFecha) {
			DateTime fecha;

			rlogger.info("Busco DatosGenerales por fecha");
			try {
				// Convierto la fecha de formato String a formato Date
				fecha = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(strFecha);

				// Realizo la consulta en datos diarios y compruebo que no este a null
				EntityDatosDiarios datosDiarios = datosDiariosService.buscarUltimoPorFecha(fecha.getDayOfMonth(),
						fecha.getMonthOfYear(), fecha.getYear());
				if (datosDiarios != null) {
					//Si no es null devuelvo el objeto encontrado
					return HerramientasMEFF.datosDiariosToDatosGenerales(datosDiarios);
				} else {
					//Si es null busco en datos mensuales y compruebo que no este a null
					EntityDatosMensuales datosMensuales = datosMensualesService.buscarUltimaPorFecha(fecha.getMonthOfYear(),
							fecha.getYear());
					if (datosMensuales != null) {
						//Si no es null devuelvo el objeto encontrado
						return HerramientasMEFF.datosMensualesToDatosGenerales(datosMensuales);
					} else {
						//Si es null busco en los datos trimestrales y compruebo que no este a null
						int trimestre = HerramientasMEFF.numeroTrimestre(fecha.getMonthOfYear());
						EntityDatosTrimestrales datosTrimestrales = datosTrimestralesService.buscarUltimaPorFecha(trimestre,
								fecha.getYear());
						if (datosTrimestrales != null) {
							//si no es null devuelvo el objeto encontrado 
							return HerramientasMEFF.datosTrimestralesToDatosGenerales(datosTrimestrales);
						} else {
							//Si es null busco en los datos anuales y compruebo que no este a null 
							EntityDatosAnuales datosAnuales = datosAnualesService.buscarUltimaPorFecha(fecha.getYear());
							if (datosAnuales != null) {
								//Si no es null devuelvo el objeto encontrado 
								return HerramientasMEFF.datosAnualesToDatosGenerales(datosAnuales);
							} else {
								//Si es null significa que no hay ningun dato con esa fecha guardado en BD
								return new ObjDatosGenerales();
							}
						}
					}

				}
			} catch (IllegalFieldValueException e) {
				// Si la fecha no tiene un formato correcto, pinto el error y devuelvo un bad
				// request
				rlogger.error("Error al formatear la fecha. \n\tFecha URL: " + strFecha);
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Formato de fecha es incorrecto", e);
			} catch (NullPointerException e) {
				return new ObjDatosGenerales();
			} catch (Exception e) {
				rlogger.error(e.getMessage());
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error desconocido", e);
			}
		}

		@CrossOrigin
		@GetMapping(path = "rangoPrecioCierre/porDia", produces = "application/json")
		@ResponseStatus(HttpStatus.CREATED)
		public List<ObjDatosGenerales> getDatosGeneralesRango(HttpServletRequest request, @RequestParam String strFechaInicio,
				@RequestParam String strFechaFin) {
			DateTime fechaInicio, fechaFin;
			List<ObjDatosGenerales> listaDatosGenerales = new ArrayList<ObjDatosGenerales>();
			rlogger.info("Busco DatosGenerales por rango fechas");
			try {
				// Convierto la fecha de formato String a formato Date
				fechaInicio = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(strFechaInicio);
				fechaFin = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(strFechaFin);

				//Recorro el rango de fechas pasado por parametro 
				for (DateTime fecha = fechaInicio; fecha.isBefore(fechaFin); fecha = fecha.plusDays(1)) {

					// Realizo la consulta en datos diarios y compruebo que no este a null
					EntityDatosDiarios datosDiarios = datosDiariosService.buscarUltimoPorFecha(fecha.getDayOfMonth(),
							fecha.getMonthOfYear(), fecha.getYear());
					if (datosDiarios != null) {
						//Si no es null devuelvo el objeto encontrado
						listaDatosGenerales.add(HerramientasMEFF.datosDiariosToDatosGenerales(datosDiarios));
					} else {
						// Realizo la consulta en datos diarios y compruebo que no este a null
						EntityDatosMensuales datosMensuales = datosMensualesService.buscarUltimaPorFecha(fecha.getMonthOfYear(),
								fecha.getYear());

						if (datosMensuales != null) {
							//Si no es null devuelvo el objeto encontrado
							listaDatosGenerales.add(HerramientasMEFF.datosMensualesToDatosGenerales(datosMensuales));
						} else {
							// Realizo la consulta en datos diarios y compruebo que no este a null
							int trimestre = HerramientasMEFF.numeroTrimestre(fecha.getMonthOfYear());
							EntityDatosTrimestrales datosTrimestrales = datosTrimestralesService.buscarUltimaPorFecha(trimestre,
									fecha.getYear());
							if (datosTrimestrales != null) {
								//Si no es null devuelvo el objeto encontrado
								listaDatosGenerales.add(HerramientasMEFF.datosTrimestralesToDatosGenerales(datosTrimestrales));
							} else {
								// Realizo la consulta en datos diarios y compruebo que no este a null
								EntityDatosAnuales datosAnuales = datosAnualesService.buscarUltimaPorFecha(fecha.getYear());
								if (datosAnuales != null) {
									//Si no es null devuelvo el objeto encontrado
									listaDatosGenerales.add(HerramientasMEFF.datosAnualesToDatosGenerales(datosAnuales));
								}
							}
						}

					}
				}
				return listaDatosGenerales;
			} catch (IllegalFieldValueException e) {
				// Si la fecha no tiene un formato correcto, pinto el error y devuelvo un bad
				// request
				rlogger.error("Error al formatear las fechas. \n\tFecha Inicio: " + strFechaInicio + " \n\tFecha Fin: "
						+ strFechaFin);
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Formato de fecha es incorrecto", e);
			} catch (NullPointerException e) {
				return new ArrayList<ObjDatosGenerales>();
			} catch (Exception e) {
				rlogger.error(e.getMessage());
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error desconocido", e);
			}
		}
}
