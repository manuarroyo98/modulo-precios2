package modulo.precios.meff;

public class ObjDatosGenerales {

	private String tipoDato;

	private Integer dia;

	private Integer semana;
	
	private Boolean finDeSemana;
	
	private Integer trimestre;
	
	private Integer mes;
	
	private Integer anio;

	
	private Double precioBase;

	private Double difBase;

	private Double porcentajeDifBase;


	private Double precioPunta;

	private Double difPunta;

	private Double porcentajeDifPunta;

	public ObjDatosGenerales() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getTipoDato() {
		return tipoDato;
	}

	public void setTipoDato(String tipoDato) {
		this.tipoDato = tipoDato;
	}

	public Integer getDia() {
		return dia;
	}

	public void setDia(Integer dia) {
		this.dia = dia;
	}

	public Integer getSemana() {
		return semana;
	}

	public void setSemana(Integer semana) {
		this.semana = semana;
	}

	public Integer getTrimestre() {
		return trimestre;
	}

	public void setTrimestre(Integer trimestre) {
		this.trimestre = trimestre;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public Integer getAnio() {
		return anio;
	}

	public void setAnio(Integer anio) {
		this.anio = anio;
	}

	public Double getPrecioBase() {
		return precioBase;
	}

	public void setPrecioBase(Double precioBase) {
		this.precioBase = precioBase;
	}

	public Double getDifBase() {
		return difBase;
	}

	public void setDifBase(Double difBase) {
		this.difBase = difBase;
	}

	public Double getPorcentajeDifBase() {
		return porcentajeDifBase;
	}

	public void setPorcentajeDifBase(Double porcentajeDifBase) {
		this.porcentajeDifBase = porcentajeDifBase;
	}

	public Double getPrecioPunta() {
		return precioPunta;
	}

	public void setPrecioPunta(Double precioPunta) {
		this.precioPunta = precioPunta;
	}

	public Double getDifPunta() {
		return difPunta;
	}

	public void setDifPunta(Double difPunta) {
		this.difPunta = difPunta;
	}

	public Double getPorcentajeDifPunta() {
		return porcentajeDifPunta;
	}

	public void setPorcentajeDifPunta(Double porcentajeDifPunta) {
		this.porcentajeDifPunta = porcentajeDifPunta;
	}

	public Boolean getFinDeSemana() {
		return finDeSemana;
	}

	public void setFinDeSemana(Boolean finDeSemana) {
		this.finDeSemana = finDeSemana;
	}
}
