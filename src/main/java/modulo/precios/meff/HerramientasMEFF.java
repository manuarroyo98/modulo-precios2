package modulo.precios.meff;

import modulo.precios.meff.entities.EntityDatosAnuales;
import modulo.precios.meff.entities.EntityDatosDiarios;
import modulo.precios.meff.entities.EntityDatosMensuales;
import modulo.precios.meff.entities.EntityDatosSemanales;
import modulo.precios.meff.entities.EntityDatosTrimestrales;

public class HerramientasMEFF {

	/**
	 * Devuelve el numero del mes pasado por string
	 * 
	 * @param mes
	 * @return
	 */
	public static int numeroMes(String mes) {
		switch (mes) {
			case "ene":
				return 1;
			case "feb":
				return 2;
			case "mar":
				return 3;
			case "abr":
				return 4;
			case "may":
				return 5;
			case "jun":
				return 6;
			case "jul":
				return 7;
			case "ago":
				return 8;
			case "sep":
				return 9;
			case "oct":
				return 10;
			case "nov":
				return 11;
			case "dic":
				return 12;
			default:
				return -1;
		}
	}

	/**
	 * Devuelve el numero del trimestre del mes pasado por parametro 
	 * @param mes
	 * @return
	 */
	public static int numeroTrimestre(int mes) {
		switch (mes) {
		case 1:
		case 2:
		case 3:
			return 1;
		case 4:
		case 5:
		case 6:
			return 2;
		case 7:
		case 8:
		case 9:
			return 3;
		case 10:
		case 11:
		case 12:
			return 4;
		}
		return 0;

	}

	/**
	 * Comprueba el numero pasado como cadena sea un valor numerico, de lo contrario devuelve un null
	 * @param numero
	 * @return
	 */
	public static Double stringToDouble(String numero) {
		try {
			numero = numero.replace(",", ".");
			return Double.parseDouble(numero);
		} catch (NumberFormatException e) {
			return null;
		}
	}

	/**
	 * Convierte un objeto de DatosDiarios a un objeto de DatosGenerales
	 * @param datosDiarios
	 * @return
	 */
	public static ObjDatosGenerales datosDiariosToDatosGenerales(EntityDatosDiarios datosDiarios) {
		ObjDatosGenerales datosGenerales = new ObjDatosGenerales();
		datosGenerales.setTipoDato("Dia");
		datosGenerales.setDia(datosDiarios.getDia());
		datosGenerales.setMes(datosDiarios.getMes());
		datosGenerales.setAnio(datosDiarios.getAnio());
		datosGenerales.setPrecioBase(datosDiarios.getPrecioBase());
		datosGenerales.setDifBase(datosDiarios.getDifBase());
		datosGenerales.setPorcentajeDifBase(datosDiarios.getPorcentajeDifBase());
		datosGenerales.setPrecioPunta(datosDiarios.getPrecioPunta());
		datosGenerales.setDifPunta(datosDiarios.getDifPunta());
		datosGenerales.setPorcentajeDifPunta(datosDiarios.getPorcentajeDifPunta());
		return datosGenerales;

	}

	/**
	 * Convierte un objeto de DatosSemanales a un objeto de DatosGenerales
	 * @param datosDiarios
	 * @return
	 */
	public static ObjDatosGenerales datosSemanalesToDatosGenerales(EntityDatosSemanales datosSemanales) {
		ObjDatosGenerales datosGenerales = new ObjDatosGenerales();
		datosGenerales.setTipoDato("Semana");
		datosGenerales.setSemana(datosSemanales.getSemana());
		datosGenerales.setFinDeSemana(datosSemanales.isFinDeSemana());
		datosGenerales.setAnio(datosSemanales.getAnio());
		datosGenerales.setPrecioBase(datosSemanales.getPrecioBase());
		datosGenerales.setDifBase(datosSemanales.getDifBase());
		datosGenerales.setPorcentajeDifBase(datosSemanales.getPorcentajeDifBase());
		datosGenerales.setPrecioPunta(datosSemanales.getPrecioPunta());
		datosGenerales.setDifPunta(datosSemanales.getDifPunta());
		datosGenerales.setPorcentajeDifPunta(datosSemanales.getPorcentajeDifPunta());
		return datosGenerales;

	}

	/**
	 * Convierte un objeto de DatosMensuales a un objeto de DatosGenerales
	 * @param datosDiarios
	 * @return
	 */
	public static ObjDatosGenerales datosMensualesToDatosGenerales(EntityDatosMensuales datosMensuales) {
		ObjDatosGenerales datosGenerales = new ObjDatosGenerales();
		datosGenerales.setTipoDato("Mes");
		datosGenerales.setMes(datosMensuales.getMes());
		datosGenerales.setAnio(datosMensuales.getAnio());
		datosGenerales.setPrecioBase(datosMensuales.getPrecioBase());
		datosGenerales.setDifBase(datosMensuales.getDifBase());
		datosGenerales.setPorcentajeDifBase(datosMensuales.getPorcentajeDifBase());
		datosGenerales.setPrecioPunta(datosMensuales.getPrecioPunta());
		datosGenerales.setDifPunta(datosMensuales.getDifPunta());
		datosGenerales.setPorcentajeDifPunta(datosMensuales.getPorcentajeDifPunta());
		return datosGenerales;

	}

	/**
	 * Convierte un objeto de DatosTrimestrales a un objeto de DatosGenerales
	 * @param datosDiarios
	 * @return
	 */
	public static ObjDatosGenerales datosTrimestralesToDatosGenerales(EntityDatosTrimestrales datosTrimestrales) {
		ObjDatosGenerales datosGenerales = new ObjDatosGenerales();
		datosGenerales.setTipoDato("Trimestre");
		datosGenerales.setTrimestre(datosTrimestrales.getTrimestre());
		datosGenerales.setAnio(datosTrimestrales.getAnio());
		datosGenerales.setPrecioBase(datosTrimestrales.getPrecioBase());
		datosGenerales.setDifBase(datosTrimestrales.getDifBase());
		datosGenerales.setPorcentajeDifBase(datosTrimestrales.getPorcentajeDifBase());
		datosGenerales.setPrecioPunta(datosTrimestrales.getPrecioPunta());
		datosGenerales.setDifPunta(datosTrimestrales.getDifPunta());
		datosGenerales.setPorcentajeDifPunta(datosTrimestrales.getPorcentajeDifPunta());
		return datosGenerales;

	}

	/**
	 * Convierte un objeto de DatosAnuales a un objeto de DatosGenerales
	 * @param datosDiarios
	 * @return
	 */
	public static ObjDatosGenerales datosAnualesToDatosGenerales(EntityDatosAnuales datosAnuales) {
		ObjDatosGenerales datosGenerales = new ObjDatosGenerales();
		datosGenerales.setTipoDato("Anual");
		datosGenerales.setAnio(datosAnuales.getAnio());
		datosGenerales.setPrecioBase(datosAnuales.getPrecioBase());
		datosGenerales.setDifBase(datosAnuales.getDifBase());
		datosGenerales.setPorcentajeDifBase(datosAnuales.getPorcentajeDifBase());
		datosGenerales.setPrecioPunta(datosAnuales.getPrecioPunta());
		datosGenerales.setDifPunta(datosAnuales.getDifPunta());
		datosGenerales.setPorcentajeDifPunta(datosAnuales.getPorcentajeDifPunta());
		return datosGenerales;

	}
}
