package modulo.precios.meff.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "datos_mensuales_meff")
public class EntityDatosMensuales {

	@Id
	@Column(name = "id_datos_mensuales")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	private int idDatosMensuales;

	@Column(name = "mes", nullable = false)
	private int mes;
	
	@Column(name = "anio", nullable = false)
	private int anio;

	@Column(name = "precioBase")
	private Double precioBase;
	@Column(name = "difBase")
	private Double difBase;
	@Column(name = "porcentajeDifBase")
	private Double porcentajeDifBase;

	@Column(name = "precioPunta")
	private Double precioPunta;
	@Column(name = "difPunta")
	private Double difPunta;
	@Column(name = "porcentajeDifPunta")
	private Double porcentajeDifPunta;
	@ManyToOne
	@JoinColumn(name = "idRegistroDiario")
	private EntityRegistroDiario registroDiario;
	

	public EntityDatosMensuales(int idDatosMensuales, int mes, int anio, double precioBase, double difBase,
			double porcentajeDifBase, double precioPunta, double difPunta, double porcentajeDifPunta,
			EntityRegistroDiario registroDiario) {
		super();
		this.idDatosMensuales = idDatosMensuales;
		this.mes = mes;
		this.anio = anio;
		this.precioBase = precioBase;
		this.difBase = difBase;
		this.porcentajeDifBase = porcentajeDifBase;
		this.precioPunta = precioPunta;
		this.difPunta = difPunta;
		this.porcentajeDifPunta = porcentajeDifPunta;
		this.registroDiario = registroDiario;
	}

	public EntityDatosMensuales() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getIdDatosMensuales() {
		return idDatosMensuales;
	}

	public void setIdDatosMensuales(int idDatosMensuales) {
		this.idDatosMensuales = idDatosMensuales;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	public Double getPrecioBase() {
		return precioBase;
	}

	public void setPrecioBase(Double precioBase) {
		this.precioBase = precioBase;
	}

	public Double getDifBase() {
		return difBase;
	}

	public void setDifBase(Double difBase) {
		this.difBase = difBase;
	}

	public Double getPorcentajeDifBase() {
		return porcentajeDifBase;
	}

	public void setPorcentajeDifBase(Double porcentajeDifBase) {
		this.porcentajeDifBase = porcentajeDifBase;
	}

	public Double getPrecioPunta() {
		return precioPunta;
	}

	public void setPrecioPunta(Double precioPunta) {
		this.precioPunta = precioPunta;
	}

	public Double getDifPunta() {
		return difPunta;
	}

	public void setDifPunta(Double difPunta) {
		this.difPunta = difPunta;
	}

	public Double getPorcentajeDifPunta() {
		return porcentajeDifPunta;
	}

	public void setPorcentajeDifPunta(Double porcentajeDifPunta) {
		this.porcentajeDifPunta = porcentajeDifPunta;
	}

	public EntityRegistroDiario getRegistroDiario() {
		return registroDiario;
	}

	public void setRegistroDiario(EntityRegistroDiario registroDiario) {
		this.registroDiario = registroDiario;
	}

	@Override
	public String toString() {
		return "DatosMensuales [idDatosMensuales=" + idDatosMensuales + ", mes=" + mes + ", anio=" + anio
				+ ", precioBase=" + precioBase + ", difBase=" + difBase + ", porcentajeDifBase=" + porcentajeDifBase
				+ ", precioPunta=" + precioPunta + ", difPunta=" + difPunta + ", porcentajeDifPunta="
				+ porcentajeDifPunta + ", registroDiario=" + registroDiario + "]";
	}
}
