package modulo.precios.meff.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "registro_diario_meff")
public class EntityRegistroDiario {

	@Id
	@Column(name = "id_registro_diario")
	@GeneratedValue
	private int idRegistroDiario;
	
	@CreatedDate
	@Temporal(TemporalType.DATE)
	@Column(name = "fecha", nullable = false)
	private Date fecha;

	@OneToMany(mappedBy = "registroDiario", cascade = CascadeType.ALL)
	@JsonIgnoreProperties("registroDiario")
	private List<EntityDatosTrimestrales> listaDatosTrimestrales;

	@OneToMany(mappedBy = "registroDiario", cascade = CascadeType.ALL)
	@JsonIgnoreProperties("registroDiario")
	private List<EntityDatosMensuales> listaDatosMensuales;
	
	@OneToMany(mappedBy = "registroDiario", cascade = CascadeType.ALL)
	@JsonIgnoreProperties("registroDiario")
	private List<EntityDatosSemanales> listaDatosSemanales;
	
	@OneToMany(mappedBy = "registroDiario", cascade = CascadeType.ALL)
	@JsonIgnoreProperties("registroDiario")
	private List<EntityDatosDiarios> listaDatosDiarios;
	
	@OneToMany(mappedBy = "registroDiario", cascade = CascadeType.ALL)
	@JsonIgnoreProperties("registroDiario")
	private List<EntityDatosAnuales> listaDatosAnuales;
	
	public EntityRegistroDiario() {
		super();
		listaDatosTrimestrales = new ArrayList<EntityDatosTrimestrales>();
		listaDatosMensuales = new ArrayList<EntityDatosMensuales>();
		listaDatosSemanales = new ArrayList<EntityDatosSemanales>();
		listaDatosDiarios = new ArrayList<EntityDatosDiarios>();
		listaDatosAnuales = new ArrayList<EntityDatosAnuales>();
	}

	public EntityRegistroDiario(int idRegistroDiario, Date fecha) {
		super();
		this.idRegistroDiario = idRegistroDiario;
		this.fecha = fecha;
		listaDatosTrimestrales = new ArrayList<EntityDatosTrimestrales>();
		listaDatosMensuales = new ArrayList<EntityDatosMensuales>();
		listaDatosSemanales = new ArrayList<EntityDatosSemanales>();
		listaDatosDiarios = new ArrayList<EntityDatosDiarios>();
		listaDatosAnuales = new ArrayList<EntityDatosAnuales>();
	}

	public int getIdRegistroDiario() {
		return idRegistroDiario;
	}

	public void setIdRegistroDiario(int idRegistroDiario) {
		this.idRegistroDiario = idRegistroDiario;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public List<EntityDatosTrimestrales> getListaDatosTrimestrales() {
		return listaDatosTrimestrales;
	}

	public void setListaDatosTrimestrales(List<EntityDatosTrimestrales> listaDatosTrimestrales) {
		this.listaDatosTrimestrales = listaDatosTrimestrales;
	}

	public List<EntityDatosMensuales> getListaDatosMensuales() {
		return listaDatosMensuales;
	}

	public void setListaDatosMensuales(List<EntityDatosMensuales> listaDatosMensuales) {
		this.listaDatosMensuales = listaDatosMensuales;
	}

	public List<EntityDatosSemanales> getListaDatosSemanales() {
		return listaDatosSemanales;
	}

	public void setListaDatosSemanales(List<EntityDatosSemanales> listaDatosSemanales) {
		this.listaDatosSemanales = listaDatosSemanales;
	}

	public List<EntityDatosDiarios> getListaDatosDiarios() {
		return listaDatosDiarios;
	}

	public void setListaDatosDiarios(List<EntityDatosDiarios> listaDatosDiarios) {
		this.listaDatosDiarios = listaDatosDiarios;
	}

	public List<EntityDatosAnuales> getListaDatosAnuales() {
		return listaDatosAnuales;
	}

	public void setListaDatosAnuales(List<EntityDatosAnuales> listaDatosAnuales) {
		this.listaDatosAnuales = listaDatosAnuales;
	}

	@Override
	public String toString() {
		return "RegistroDiario [idRegistroDiario=" + idRegistroDiario + ", fecha=" + fecha + ", listaDatosTrimestrales="
				+ listaDatosTrimestrales + ", listaDatosMensuales=" + listaDatosMensuales + ", listaDatosSemanales="
				+ listaDatosSemanales + ", listaDatosDiarios=" + listaDatosDiarios + ", listaDatosAnuales="
				+ listaDatosAnuales + "]";
	}
}
