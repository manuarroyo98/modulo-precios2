package modulo.precios.meff.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "datos_diarios_meff")
public class EntityDatosDiarios {

	@Id
	@Column(name = "id_datos_diarios")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	private int idDatosDiarios;

	@Column(name = "dia", nullable = false)
	private int dia;
	
	@Column(name = "mes", nullable = false)
	private int mes;
	
	@Column(name = "anio", nullable = false)
	private int anio;

	@Column(name = "precioBase")
	private Double precioBase;
	@Column(name = "difBase")
	private Double difBase;
	@Column(name = "porcentajeDifBase")
	private Double porcentajeDifBase;

	@Column(name = "precioPunta")
	private Double precioPunta;
	@Column(name = "difPunta")
	private Double difPunta;
	@Column(name = "porcentajeDifPunta")
	private Double porcentajeDifPunta;
	@ManyToOne
	@JoinColumn(name = "idRegistroDiario")
	private EntityRegistroDiario registroDiario;
	
	public EntityDatosDiarios() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EntityDatosDiarios(int idDatosDiarios, int dia, int mes, int anio, double precioBase, double difBase,
			double porcentajeDifBase, double precioPunta, double difPunta, double porcentajeDifPunta,
			EntityRegistroDiario registroDiario) {
		super();
		this.idDatosDiarios = idDatosDiarios;
		this.dia = dia;
		this.mes = mes;
		this.anio = anio;
		this.precioBase = precioBase;
		this.difBase = difBase;
		this.porcentajeDifBase = porcentajeDifBase;
		this.precioPunta = precioPunta;
		this.difPunta = difPunta;
		this.porcentajeDifPunta = porcentajeDifPunta;
		this.registroDiario = registroDiario;
	}

	public int getIdDatosDiarios() {
		return idDatosDiarios;
	}

	public void setIdDatosDiarios(int idDatosDiarios) {
		this.idDatosDiarios = idDatosDiarios;
	}

	public int getDia() {
		return dia;
	}

	public void setDia(int dia) {
		this.dia = dia;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	public Double getPrecioBase() {
		return precioBase;
	}

	public void setPrecioBase(Double precioBase) {
		this.precioBase = precioBase;
	}

	public Double getDifBase() {
		return difBase;
	}

	public void setDifBase(Double difBase) {
		this.difBase = difBase;
	}

	public Double getPorcentajeDifBase() {
		return porcentajeDifBase;
	}

	public void setPorcentajeDifBase(Double porcentajeDifBase) {
		this.porcentajeDifBase = porcentajeDifBase;
	}

	public Double getPrecioPunta() {
		return precioPunta;
	}

	public void setPrecioPunta(Double precioPunta) {
		this.precioPunta = precioPunta;
	}

	public Double getDifPunta() {
		return difPunta;
	}

	public void setDifPunta(Double difPunta) {
		this.difPunta = difPunta;
	}

	public Double getPorcentajeDifPunta() {
		return porcentajeDifPunta;
	}

	public void setPorcentajeDifPunta(Double porcentajeDifPunta) {
		this.porcentajeDifPunta = porcentajeDifPunta;
	}

	public EntityRegistroDiario getRegistroDiario() {
		return registroDiario;
	}

	public void setRegistroDiario(EntityRegistroDiario registroDiario) {
		this.registroDiario = registroDiario;
	}

	@Override
	public String toString() {
		return "DatosDiarios [idDatosDiarios=" + idDatosDiarios + ", dia=" + dia + ", mes=" + mes + ", anio=" + anio
				+ ", precioBase=" + precioBase + ", difBase=" + difBase + ", porcentajeDifBase=" + porcentajeDifBase
				+ ", precioPunta=" + precioPunta + ", difPunta=" + difPunta + ", porcentajeDifPunta="
				+ porcentajeDifPunta + ", registroDiario=" + registroDiario + "]";
	}
}
