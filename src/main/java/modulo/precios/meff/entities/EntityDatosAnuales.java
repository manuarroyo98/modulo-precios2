package modulo.precios.meff.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "datos_anuales_meff")
public class EntityDatosAnuales {

	@Id
	@Column(name = "id_datos_anuales")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	private int idDatosAnuales;

	@Column(name = "anio", nullable = false)
	private int anio;

	@Column(name = "precioBase")
	private Double precioBase;
	@Column(name = "difBase")
	private Double difBase;
	@Column(name = "porcentajeDifBase")
	private Double porcentajeDifBase;

	@Column(name = "precioPunta")
	private Double precioPunta;
	@Column(name = "difPunta")
	private Double difPunta;
	@Column(name = "porcentajeDifPunta")
	private Double porcentajeDifPunta;

	@ManyToOne
	@JoinColumn(name = "idRegistroDiario")
	private EntityRegistroDiario registroDiario;

	public EntityDatosAnuales(int idDatosAnuales, int anio, double precioBase, double difBase, double porcentajeDifBase,
			double precioPunta, double difPunta, double porcentajeDifPunta, EntityRegistroDiario registroDiario) {
		super();
		this.idDatosAnuales = idDatosAnuales;
		this.anio = anio;
		this.precioBase = precioBase;
		this.difBase = difBase;
		this.porcentajeDifBase = porcentajeDifBase;
		this.precioPunta = precioPunta;
		this.difPunta = difPunta;
		this.porcentajeDifPunta = porcentajeDifPunta;
		this.registroDiario = registroDiario;
	}

	public EntityDatosAnuales() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getIdDatosAnuales() {
		return idDatosAnuales;
	}

	public void setIdDatosAnuales(int idDatosAnuales) {
		this.idDatosAnuales = idDatosAnuales;
	}

	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	public Double getPrecioBase() {
		return precioBase;
	}

	public void setPrecioBase(Double precioBase) {
		this.precioBase = precioBase;
	}

	public Double getDifBase() {
		return difBase;
	}

	public void setDifBase(Double difBase) {
		this.difBase = difBase;
	}

	public Double getPorcentajeDifBase() {
		return porcentajeDifBase;
	}

	public void setPorcentajeDifBase(Double porcentajeDifBase) {
		this.porcentajeDifBase = porcentajeDifBase;
	}

	public Double getPrecioPunta() {
		return precioPunta;
	}

	public void setPrecioPunta(Double precioPunta) {
		this.precioPunta = precioPunta;
	}

	public Double getDifPunta() {
		return difPunta;
	}

	public void setDifPunta(Double difPunta) {
		this.difPunta = difPunta;
	}

	public Double getPorcentajeDifPunta() {
		return porcentajeDifPunta;
	}

	public void setPorcentajeDifPunta(Double porcentajeDifPunta) {
		this.porcentajeDifPunta = porcentajeDifPunta;
	}

	public EntityRegistroDiario getRegistroDiario() {
		return registroDiario;
	}

	public void setRegistroDiario(EntityRegistroDiario registroDiario) {
		this.registroDiario = registroDiario;
	}

	@Override
	public String toString() {
		return "DatosAnuales [idDatosAnuales=" + idDatosAnuales + ", anio=" + anio + ", precioBase=" + precioBase
				+ ", difBase=" + difBase + ", porcentajeDifBase=" + porcentajeDifBase + ", precioPunta=" + precioPunta
				+ ", difPunta=" + difPunta + ", porcentajeDifPunta=" + porcentajeDifPunta + ", registroDiario="
				+ registroDiario + "]";
	}
}
