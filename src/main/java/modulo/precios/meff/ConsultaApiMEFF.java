package modulo.precios.meff;

import java.io.IOException;

import org.joda.time.DateTime;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import modulo.precios.meff.entities.EntityDatosAnuales;
import modulo.precios.meff.entities.EntityDatosDiarios;
import modulo.precios.meff.entities.EntityDatosMensuales;
import modulo.precios.meff.entities.EntityDatosSemanales;
import modulo.precios.meff.entities.EntityDatosTrimestrales;
import modulo.precios.meff.entities.EntityRegistroDiario;

public class ConsultaApiMEFF {

	
	private static final Logger rlogger = LoggerFactory.getLogger(ConsultaApiMEFF.class);

	/**
	 * Busca los datos en la pagina de meff y rellena un objeto EntityRegistroDiario con
	 * todos los datos de cierre de ese dia
	 * 
	 * NOTA: Aunque en el enlace ponga /Excel es un documento .xls pero en verdad es
	 * un documento HTML
	 * 
	 * @param idRegistroDiario proximo registro en BD
	 * @return EntityRegistroDiario
	 */
	public static EntityRegistroDiario consultarPreciosCierre(int idRegistroDiario, DateTime fechaRegistro) {
		// Objeto con su id de BD y la fecha del equipo en ese momento
		EntityRegistroDiario registroDiario = new EntityRegistroDiario(idRegistroDiario, fechaRegistro.toDate());
		// Ruta donde se van a extraer los datos
		String ruta = "https://www.meff.es/esp/Derivados-Commodities/Precios-Cierre/Excel";

		Document doc;
		try {
			// Conectamos y extraemos el documento
			doc = Jsoup.connect(ruta).ignoreContentType(true).get();

			// Busco los elementos con el class -> tr.text-right, esto me da una lista de
			// elementos
			Elements listTrs = doc.select("tr.text-right");
			// Recorro la lista de elementos recogida
			for (Element tr : listTrs) {
				// Busco los elementos -> td, esto me da una lista de elementos
				Elements listTd = tr.select("td");

				// Tendremos 9 td's por cada registro en el que cada uno tenemos los datos
				
				//El campo de la fecha indica si es dia,mes... separado la fecha
				String[] fecha = listTd.get(0).text().split(" ");
				//Desgrego la fecha por los giones para tener los valores por separado 
				String[] partesFecha = fecha[1].split("-");
				//Recojo los valores 
				String precioBase = listTd.get(2).text();
				String difBase = listTd.get(3).text();
				String porcenDifBase = listTd.get(4).text();

				String precioPunta = listTd.get(6).text();
				String difPunta = listTd.get(7).text();
				String porcenDifPunta = listTd.get(8).text();

				//Dependiendo si es dia,semana,fin de semana, lo guardo en un objeto y otro y lo añado a su lista 
				switch (fecha[0]) {
				case "Día":
					EntityDatosDiarios datosDiarios = new EntityDatosDiarios();
					datosDiarios.setRegistroDiario(registroDiario);
					datosDiarios.setDia(Integer.parseInt(partesFecha[0]));
					datosDiarios.setMes(HerramientasMEFF.numeroMes(partesFecha[1]));
					datosDiarios.setAnio(Integer.parseInt(partesFecha[2]));
					datosDiarios.setPrecioBase(HerramientasMEFF.stringToDouble(precioBase));
					datosDiarios.setDifBase(HerramientasMEFF.stringToDouble(difBase));
					datosDiarios.setPorcentajeDifBase(HerramientasMEFF.stringToDouble(porcenDifBase));
					datosDiarios.setPrecioPunta(HerramientasMEFF.stringToDouble(precioPunta));
					datosDiarios.setDifPunta(HerramientasMEFF.stringToDouble(difPunta));
					datosDiarios.setPorcentajeDifPunta(HerramientasMEFF.stringToDouble(porcenDifPunta));
					registroDiario.getListaDatosDiarios().add(datosDiarios);
					break;
				case "Fin de Semana":
				case "Semana":
					EntityDatosSemanales datosSemanales = new EntityDatosSemanales();
					datosSemanales.setRegistroDiario(registroDiario);
					if (fecha[0].contentEquals("Semana")) {
						datosSemanales.setFinDeSemana(false);
					} else {
						datosSemanales.setFinDeSemana(true);
					}
					datosSemanales.setSemana(Integer.parseInt(partesFecha[0]));
					datosSemanales.setAnio(Integer.parseInt(partesFecha[1]));
					datosSemanales.setPrecioBase(HerramientasMEFF.stringToDouble(precioBase));
					datosSemanales.setDifBase(HerramientasMEFF.stringToDouble(difBase));
					datosSemanales.setPorcentajeDifBase(HerramientasMEFF.stringToDouble(porcenDifBase));
					datosSemanales.setPrecioPunta(HerramientasMEFF.stringToDouble(precioPunta));
					datosSemanales.setDifPunta(HerramientasMEFF.stringToDouble(difPunta));
					datosSemanales.setPorcentajeDifPunta(HerramientasMEFF.stringToDouble(porcenDifPunta));
					registroDiario.getListaDatosSemanales().add(datosSemanales);
					break;
				case "Mes":
					EntityDatosMensuales datosMensuales = new EntityDatosMensuales();
					datosMensuales.setRegistroDiario(registroDiario);
					datosMensuales.setMes(HerramientasMEFF.numeroMes(partesFecha[0]));
					datosMensuales.setAnio(Integer.parseInt(partesFecha[1]));
					datosMensuales.setPrecioBase(HerramientasMEFF.stringToDouble(precioBase));
					datosMensuales.setDifBase(HerramientasMEFF.stringToDouble(difBase));
					datosMensuales.setPorcentajeDifBase(HerramientasMEFF.stringToDouble(porcenDifBase));
					datosMensuales.setPrecioPunta(HerramientasMEFF.stringToDouble(precioPunta));
					datosMensuales.setDifPunta(HerramientasMEFF.stringToDouble(difPunta));
					datosMensuales.setPorcentajeDifPunta(HerramientasMEFF.stringToDouble(porcenDifPunta));
					registroDiario.getListaDatosMensuales().add(datosMensuales);
					break;
				case "Trimestre":
					EntityDatosTrimestrales datosTrimestrales = new EntityDatosTrimestrales();
					datosTrimestrales.setRegistroDiario(registroDiario);
					datosTrimestrales.setTrimestre(Integer.parseInt(partesFecha[0]));
					datosTrimestrales.setAnio(Integer.parseInt(partesFecha[1]));
					datosTrimestrales.setPrecioBase(HerramientasMEFF.stringToDouble(precioBase));
					datosTrimestrales.setDifBase(HerramientasMEFF.stringToDouble(difBase));
					datosTrimestrales.setPorcentajeDifBase(HerramientasMEFF.stringToDouble(porcenDifBase));
					datosTrimestrales.setPrecioPunta(HerramientasMEFF.stringToDouble(precioPunta));
					datosTrimestrales.setDifPunta(HerramientasMEFF.stringToDouble(difPunta));
					datosTrimestrales.setPorcentajeDifPunta(HerramientasMEFF.stringToDouble(porcenDifPunta));
					registroDiario.getListaDatosTrimestrales().add(datosTrimestrales);
					break;
				case "Año":
					EntityDatosAnuales datosAnuales = new EntityDatosAnuales();
					datosAnuales.setRegistroDiario(registroDiario);
					datosAnuales.setAnio(Integer.parseInt(partesFecha[0]));
					datosAnuales.setPrecioBase(HerramientasMEFF.stringToDouble(precioBase));
					datosAnuales.setDifBase(HerramientasMEFF.stringToDouble(difBase));
					datosAnuales.setPorcentajeDifBase(HerramientasMEFF.stringToDouble(porcenDifBase));
					datosAnuales.setPrecioPunta(HerramientasMEFF.stringToDouble(precioPunta));
					datosAnuales.setDifPunta(HerramientasMEFF.stringToDouble(difPunta));
					datosAnuales.setPorcentajeDifPunta(HerramientasMEFF.stringToDouble(porcenDifPunta));
					registroDiario.getListaDatosAnuales().add(datosAnuales);
					break;
				}

			}
			//devuelvo el objeto registroDiario con los valores rellenos 
			return registroDiario;
		} catch (IOException e) {
			rlogger.error("Error al abrir el documento" + e);
			return new EntityRegistroDiario();
		}
	}
}
