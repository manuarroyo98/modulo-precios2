package modulo.precios.meff.services;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import modulo.precios.meff.entities.EntityRegistroDiario;
import modulo.precios.meff.repositories.RepositoryRegistroDiario;

@Service
public class ServiceRegistroDiario {

	private final RepositoryRegistroDiario registroDiarioRepository;

	public ServiceRegistroDiario(RepositoryRegistroDiario registroDiarioRepository) {
		this.registroDiarioRepository = registroDiarioRepository;
	}

	public List<EntityRegistroDiario> listarTodos() {
		return registroDiarioRepository.findAll();
	}

	public EntityRegistroDiario buscarPorId(int id) {
		return registroDiarioRepository.findById(id).get();
	}

	public int proximoId() {
		EntityRegistroDiario registroDiario = registroDiarioRepository.findLatestReg();
		if (registroDiario == null) {
			return 0 + 1;
		} else {
			return registroDiario.getIdRegistroDiario() + 1;
		}
	}

	public List<EntityRegistroDiario> buscarEntreFechas(Date fechaInicio, Date fechaFin) {
		return registroDiarioRepository.findBetweenDates(fechaInicio, fechaFin);
	}

	public EntityRegistroDiario buscarPorFecha(Date fecha) {
		return registroDiarioRepository.findByFecha(fecha);
	}

	public int save(EntityRegistroDiario registroDiario) {
		return registroDiarioRepository.save(registroDiario).getIdRegistroDiario();
	}
}
