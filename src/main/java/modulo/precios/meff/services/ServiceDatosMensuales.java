package modulo.precios.meff.services;

import java.util.List;

import org.springframework.stereotype.Service;

import modulo.precios.meff.entities.EntityDatosMensuales;
import modulo.precios.meff.repositories.RepositoryDatosMensuales;

@Service
public class ServiceDatosMensuales {

	private final RepositoryDatosMensuales datosMensualesRepository;

	public ServiceDatosMensuales(RepositoryDatosMensuales datosMensualesRepository) {
		this.datosMensualesRepository = datosMensualesRepository;
	}

	public List<EntityDatosMensuales> listarTodos() {
		return datosMensualesRepository.findAll();
	}

	public EntityDatosMensuales buscarPorId(int id) {
		return datosMensualesRepository.findById(id).get();
	}
	
	public EntityDatosMensuales buscarUltimaPorFecha(int mes, int anio) {
		return datosMensualesRepository.findByFechaLast(mes, anio);
	}

	public EntityDatosMensuales save(EntityDatosMensuales datosMensuales) {
		return datosMensualesRepository.save(datosMensuales);
	}
}
