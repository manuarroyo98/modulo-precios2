package modulo.precios.meff.services;

import java.util.List;

import org.springframework.stereotype.Service;

import modulo.precios.meff.entities.EntityDatosAnuales;
import modulo.precios.meff.repositories.RepositoryDatosAnuales;

@Service
public class ServiceDatosAnuales {

	private final RepositoryDatosAnuales datosAnualesRepository;

	public ServiceDatosAnuales(RepositoryDatosAnuales datosAnualesRepository) {
		this.datosAnualesRepository = datosAnualesRepository;
	}

	public List<EntityDatosAnuales> listarTodos() {
		return datosAnualesRepository.findAll();
	}

	public EntityDatosAnuales buscarPorId(int id) {
		return datosAnualesRepository.findById(id).get();
	}
	
	public EntityDatosAnuales buscarUltimaPorFecha(int anio) {
		return datosAnualesRepository.findByFechaLast(anio);
	}

	public EntityDatosAnuales save(EntityDatosAnuales datosAnuales) {
		return datosAnualesRepository.save(datosAnuales);
	}
}
