package modulo.precios.meff.services;

import java.util.List;

import org.springframework.stereotype.Service;

import modulo.precios.meff.entities.EntityDatosSemanales;
import modulo.precios.meff.repositories.RepositoryDatosSemanales;

@Service
public class ServiceDatosSemanales {

	private final RepositoryDatosSemanales datosSemanalesRepository;

	public ServiceDatosSemanales(RepositoryDatosSemanales datosSemanalesRepository) {
		this.datosSemanalesRepository = datosSemanalesRepository;
	}

	public List<EntityDatosSemanales> listarTodos() {
		return datosSemanalesRepository.findAll();
	}

	public EntityDatosSemanales buscarPorId(int id) {
		return datosSemanalesRepository.findById(id).get();
	}
	
	public EntityDatosSemanales buscarUltimaPorFecha(int semana, int anio, boolean finDeSemana) {
		return datosSemanalesRepository.findByFechaLast(semana, anio, finDeSemana);
	}

	public EntityDatosSemanales save(EntityDatosSemanales datosSemanales) {
		return datosSemanalesRepository.save(datosSemanales);
	}
}
