package modulo.precios.meff.services;

import java.util.List;

import org.springframework.stereotype.Service;

import modulo.precios.meff.entities.EntityDatosTrimestrales;
import modulo.precios.meff.repositories.RepositoryDatosTrimestrales;

@Service
public class ServiceDatosTrimestrales {

	private final RepositoryDatosTrimestrales datosTrimestralesRepository;

	public ServiceDatosTrimestrales(RepositoryDatosTrimestrales datosTrimestralesRepository) {
		this.datosTrimestralesRepository = datosTrimestralesRepository;
	}

	public List<EntityDatosTrimestrales> listarTodos() {
		return datosTrimestralesRepository.findAll();
	}

	public EntityDatosTrimestrales buscarPorId(int id) {
		return datosTrimestralesRepository.findById(id).get();
	}
	
	public EntityDatosTrimestrales buscarUltimaPorFecha(int trimestre, int anio) {
		return datosTrimestralesRepository.findByFechaLast(trimestre, anio);
	}
	
	public EntityDatosTrimestrales save(EntityDatosTrimestrales datosTrimestrales) {
		return datosTrimestralesRepository.save(datosTrimestrales);
	}
}
