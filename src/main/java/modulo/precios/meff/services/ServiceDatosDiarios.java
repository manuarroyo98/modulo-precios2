package modulo.precios.meff.services;

import java.util.List;

import org.springframework.stereotype.Service;

import modulo.precios.meff.entities.EntityDatosDiarios;
import modulo.precios.meff.repositories.RepositoryDatosDiarios;

@Service
public class ServiceDatosDiarios {

	private final RepositoryDatosDiarios datosDiariosRepository;

	public ServiceDatosDiarios(RepositoryDatosDiarios datosDiariosRepository) {
		this.datosDiariosRepository = datosDiariosRepository;
	}

	public List<EntityDatosDiarios> listarTodos() {
		return datosDiariosRepository.findAll();
	}

	public EntityDatosDiarios buscarPorId(int id) {
		return datosDiariosRepository.findById(id).get();
	}
	
	public EntityDatosDiarios buscarUltimoPorFecha(int dia,int mes,int anio) {
		return datosDiariosRepository.findByFechaLast(dia, mes, anio);
	}

	public EntityDatosDiarios save(EntityDatosDiarios datosDiarios) {
		return datosDiariosRepository.save(datosDiarios);
	}
}
