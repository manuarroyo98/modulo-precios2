package modulo.precios.meff;

import java.util.Date;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import modulo.precios.meff.entities.EntityRegistroDiario;
import modulo.precios.meff.services.ServiceRegistroDiario;

@Component
@Controller
public class RegistroDiarioMEFF {
	
	
	private static final Logger rlogger = LoggerFactory.getLogger(RegistroDiarioMEFF.class);

	

	@Autowired
	private ServiceRegistroDiario registroDiarioService;

	/**
	 * Fecha del ultimo dato Guardado
	 */
	private static DateTime fechaGuardadoPerdidas = null;

	/**
	 * Indica el estado de la insercion de los datos: -1: El programa acaba de
	 * iniciarse 1: El programa aun no ha registrado el dato del dia 2: El programa
	 * ya ha guardado el dato del dia
	 */
	private static int finTrabajoPerdidas = -1;

	/**
	 * Registra los precios de cierre diarios en BD
	 * 
	 * Este metodo se ejecuta tantas veces como el tiempo que tenga en la variable
	 * "fixedRate" el valor es en milisegundos
	 */
	@Scheduled(fixedRate = 2 * 60 * 60 * 1000)
	public void registroPreciosCierre() {
		rlogger.info("Inicio Registro Precios de Cierre");
		// Recojo la fecha actual
		DateTime fechaActual = new DateTime(new Date());
		fechaActual = fechaActual.minusDays(1);
		// Si ha cambiado el año reinicio el estado
		if (fechaGuardadoPerdidas != null) {
			if (!fechaGuardadoPerdidas.toLocalDate().isEqual(fechaActual.toLocalDate())) {
				// si estoy en diferentes dias mando que se guarde en base de datos
				finTrabajoPerdidas = 1;
				rlogger.info("Cambio de dia");
			}
		}
		// Que tipo de registro realizo
		switch (finTrabajoPerdidas) {
		case -1:
			rlogger.info("Comprobacion de inicio de aplicacion");
			// Al ser el inicio de la aplicacion busco si ya existe un registro para este
			// año
			EntityRegistroDiario registroDiario = registroDiarioService.buscarPorFecha(fechaActual.toDate());
			// si no hay un registro me devolvera null
			if (registroDiario == null) {
				try {
					int idRegistroDiario = registroDiarioService.proximoId();
					// Consulto en la API de ESIOS el registro para la fecha actual
					registroDiario = ConsultaApiMEFF.consultarPreciosCierre(idRegistroDiario, fechaActual);
					// Guardo en BD
					registroDiarioService.save(registroDiario);

					// Cambio el estado
					finTrabajoPerdidas = 2;
					rlogger.info("Datos Guardados");
				} catch (Exception e) {
					// Si no se ha registrado por algun error le pongo el estado
					finTrabajoPerdidas = 1;
					rlogger.error("Error al Guardar Datos: " + e);
				}
			} else {
				// Si ya estan registrados se lo indico en el estado
				rlogger.info("Datos ya estan Guardados");
				finTrabajoPerdidas = 2;
			}

			break;
		case 1:
			rlogger.info("Guardar nuevos datos");
			try {
				int idRegistroDiario = registroDiarioService.proximoId();
				// Consulto en la API de ESIOS el registro para la fecha actual
				registroDiario = ConsultaApiMEFF.consultarPreciosCierre(idRegistroDiario, fechaActual);
				// Guardo en BD
				registroDiarioService.save(registroDiario);

				// Cambio el estado
				finTrabajoPerdidas = 2;
				rlogger.info("Datos Guardados");
			} catch (Exception e) {
				// Si no se ha registrado por algun error le pongo el estado
				finTrabajoPerdidas = 1;
				rlogger.error("Error al Guardar Datos: " + e);
			}

			break;
		}

		// Registro la fecha actual en el historial
		fechaGuardadoPerdidas = fechaActual;
		rlogger.info("Fin del metodo");
	}
}
