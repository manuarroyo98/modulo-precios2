package modulo.precios.meff.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import modulo.precios.meff.entities.EntityDatosMensuales;

public interface RepositoryDatosMensuales extends JpaRepository<EntityDatosMensuales, Integer>{
	
	@Query(value = "SELECT * FROM datos_mensuales_meff WHERE mes = ?1 and anio = ?2 order by id_datos_mensuales desc limit 1", nativeQuery = true)
	EntityDatosMensuales findByFechaLast(int mes, int anio);

}
