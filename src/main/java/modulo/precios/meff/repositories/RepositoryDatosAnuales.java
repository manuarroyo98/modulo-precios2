package modulo.precios.meff.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import modulo.precios.meff.entities.EntityDatosAnuales;

public interface RepositoryDatosAnuales extends JpaRepository<EntityDatosAnuales, Integer>{
	
	@Query(value = "SELECT * FROM datos_anuales_meff WHERE anio = ?1 order by id_datos_anuales desc limit 1", nativeQuery = true)
	EntityDatosAnuales findByFechaLast(int anio);

}
