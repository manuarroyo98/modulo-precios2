package modulo.precios.meff.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import modulo.precios.meff.entities.EntityDatosDiarios;

public interface RepositoryDatosDiarios extends JpaRepository<EntityDatosDiarios, Integer>{
	
	@Query(value = "SELECT * FROM datos_diarios_meff WHERE dia = ?1 and mes = ?2 and anio = ?3 order by id_datos_diarios desc limit 1", nativeQuery = true)
	EntityDatosDiarios findByFechaLast(int dia,int mes, int anio);

}
