package modulo.precios.meff.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import modulo.precios.meff.entities.EntityDatosTrimestrales;

public interface RepositoryDatosTrimestrales extends JpaRepository<EntityDatosTrimestrales, Integer>{
	
	@Query(value = "SELECT * FROM datos_trimestrales_meff WHERE trimestre = ?1 and anio = ?2 order by id_datos_trimestrales desc limit 1", nativeQuery = true)
	EntityDatosTrimestrales findByFechaLast(int trimestre, int anio);

}
