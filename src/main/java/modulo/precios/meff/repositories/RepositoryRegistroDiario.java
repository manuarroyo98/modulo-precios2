package modulo.precios.meff.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import modulo.precios.meff.entities.EntityRegistroDiario;

public interface RepositoryRegistroDiario extends JpaRepository<EntityRegistroDiario, Integer>{
	
	@Query(value = "SELECT * FROM registro_diario_meff WHERE fecha between ?1 and ?2 ", nativeQuery = true)
	List<EntityRegistroDiario> findBetweenDates(Date fechaInicio,Date fechaFin);
	
	@Query(value = "SELECT * FROM registro_diario_meff order by fecha desc limit 1", nativeQuery = true)
	EntityRegistroDiario findLatestReg();
	
	EntityRegistroDiario findByFecha(Date fecha);

}
