package modulo.precios.meff.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import modulo.precios.meff.entities.EntityDatosSemanales;

public interface RepositoryDatosSemanales extends JpaRepository<EntityDatosSemanales, Integer>{
	
	@Query(value = "SELECT * FROM datos_semanales_meff WHERE semana = ?1 and anio = ?2 and fin_de_semana = ?3 order by id_datos_semanales desc limit 1", nativeQuery = true)
	EntityDatosSemanales findByFechaLast(int semana, int anio, boolean finDeSemana);

}
