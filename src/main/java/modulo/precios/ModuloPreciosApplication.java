package modulo.precios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages = "modulo.precios")
@EnableScheduling
public class ModuloPreciosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ModuloPreciosApplication.class, args);
	}

}
