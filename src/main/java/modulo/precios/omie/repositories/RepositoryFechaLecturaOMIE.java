package modulo.precios.omie.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;

import modulo.precios.omie.entities.EntityFechaLecturaOMIE;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositoryFechaLecturaOMIE extends JpaRepository<EntityFechaLecturaOMIE, Integer> {

	@Query(value = "SELECT * FROM fecha_lectura_omie WHERE fecha between ?1 and ?2 order by fecha asc", nativeQuery = true)
	List<EntityFechaLecturaOMIE> findBetweenDates(Date fechaInicio,Date fechaFin);
	
	EntityFechaLecturaOMIE findByFecha(Date fecha);
	
	@Query(value = "SELECT fecha FROM fecha_lectura_omie ORDER BY fecha ASC LIMIT 1", nativeQuery = true)
	String fechaPrimerRegistro();
	
	@Query(value = "SELECT fecha FROM fecha_lectura_omie ORDER BY fecha DESC LIMIT 1", nativeQuery = true)
	String fechaUltimoRegistro();
}
