package modulo.precios.omie.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import modulo.precios.omie.entities.EntityFestivoOMIE;

public interface RepositoryFestivoOMIE extends JpaRepository<EntityFestivoOMIE, Integer>{

	@Query(value = "SELECT * FROM festivo_omie WHERE YEAR(fecha) = ?1 order by fecha asc", nativeQuery = true)
	List<EntityFestivoOMIE> findFechaYear(int year);
}
