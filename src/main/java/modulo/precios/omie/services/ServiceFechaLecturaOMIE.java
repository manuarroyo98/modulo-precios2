package modulo.precios.omie.services;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import modulo.precios.omie.entities.EntityFechaLecturaOMIE;
import modulo.precios.omie.repositories.RepositoryFechaLecturaOMIE;

@Service
public class ServiceFechaLecturaOMIE {

	private final RepositoryFechaLecturaOMIE repositoryFechaLecturaOMIE;

	public ServiceFechaLecturaOMIE(RepositoryFechaLecturaOMIE repositoryFechaLecturaOMIE) {
		this.repositoryFechaLecturaOMIE = repositoryFechaLecturaOMIE;
	}

	public List<EntityFechaLecturaOMIE> listarTodos() {
		return repositoryFechaLecturaOMIE.findAll();
	}

	public EntityFechaLecturaOMIE buscarPorId(int id) {
		return repositoryFechaLecturaOMIE.findById(id).get();
	}
	
	public EntityFechaLecturaOMIE buscarPorFecha(Date fecha) {
		return repositoryFechaLecturaOMIE.findByFecha(fecha);
	}
	
	public List<EntityFechaLecturaOMIE> buscarEntreFechas(Date fechaInicio,Date fechaFin) {
		return repositoryFechaLecturaOMIE.findBetweenDates(fechaInicio, fechaFin);
	}

	public int save(EntityFechaLecturaOMIE EntityFechaLecturaOMIE) {
		return repositoryFechaLecturaOMIE.save(EntityFechaLecturaOMIE).getIdFechaLectura();
	}
	
	public String fechaPrimerRegistro() {
		return repositoryFechaLecturaOMIE.fechaPrimerRegistro();
	}
	
	public String fechaUltimoRegistro() {
		return repositoryFechaLecturaOMIE.fechaUltimoRegistro();
	}
}
