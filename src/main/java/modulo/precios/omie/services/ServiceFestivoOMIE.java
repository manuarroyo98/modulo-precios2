package modulo.precios.omie.services;

import java.util.List;

import org.springframework.stereotype.Service;

import modulo.precios.omie.entities.EntityFestivoOMIE;
import modulo.precios.omie.repositories.RepositoryFestivoOMIE;

@Service
public class ServiceFestivoOMIE {

	private final RepositoryFestivoOMIE RepositoryFestivoOMIE;

	public ServiceFestivoOMIE(RepositoryFestivoOMIE RepositoryFestivoOMIE) {
		this.RepositoryFestivoOMIE = RepositoryFestivoOMIE;
	}

	public List<EntityFestivoOMIE> listarTodos() {
		return RepositoryFestivoOMIE.findAll();
	}

	public EntityFestivoOMIE buscarPorId(int id) {
		return RepositoryFestivoOMIE.findById(id).get();
	}
	
	public List<EntityFestivoOMIE> buscarPorAnio(int anio) {
		return RepositoryFestivoOMIE.findFechaYear(anio);
	}

	public int save(EntityFestivoOMIE EntityFestivoOMIE) {
		return RepositoryFestivoOMIE.save(EntityFestivoOMIE).getIdFestivo();
	}
}
