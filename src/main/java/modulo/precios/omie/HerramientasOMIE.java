package modulo.precios.omie;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import modulo.precios.omie.entities.EntityFechaLecturaOMIE;

/**
 * Esta clase contiene todos los metodos de consulta a URLs
 * La transformacion del objeto FechaLectura de formato vector a formato de columna
 * @author Diego Fernández Díaz
 * @version 1.0
 * @since 13/07/2020
 *
 */
public class HerramientasOMIE {

	
	private static final Logger rlogger = LoggerFactory.getLogger(HerramientasOMIE.class);
	/**
	 * Convierte un objeto FechaLectura a un objeto FechaLecturaListada
	 * 
	 * @param fechaLectura
	 * @return FechaLecturaListada
	 */
	public static ObjFechaLecturaListada formatearFechaLectura(EntityFechaLecturaOMIE fechaLectura) {
		ObjFechaLecturaListada fechaLecturaListada = new ObjFechaLecturaListada();
		//System.out.println(fechaLectura.toString());
		fechaLecturaListada.setFecha(fechaLectura.getFecha());
		fechaLecturaListada.getListaPrecios().put(1, fechaLectura.getH1());
		fechaLecturaListada.getListaPrecios().put(2, fechaLectura.getH2());
		fechaLecturaListada.getListaPrecios().put(3, fechaLectura.getH3());
		fechaLecturaListada.getListaPrecios().put(4, fechaLectura.getH4());
		fechaLecturaListada.getListaPrecios().put(5, fechaLectura.getH5());
		fechaLecturaListada.getListaPrecios().put(6, fechaLectura.getH6());
		fechaLecturaListada.getListaPrecios().put(7, fechaLectura.getH7());
		fechaLecturaListada.getListaPrecios().put(8, fechaLectura.getH8());
		fechaLecturaListada.getListaPrecios().put(9, fechaLectura.getH9());
		fechaLecturaListada.getListaPrecios().put(10, fechaLectura.getH10());
		fechaLecturaListada.getListaPrecios().put(11, fechaLectura.getH11());
		fechaLecturaListada.getListaPrecios().put(12, fechaLectura.getH12());
		fechaLecturaListada.getListaPrecios().put(13, fechaLectura.getH13());
		fechaLecturaListada.getListaPrecios().put(14, fechaLectura.getH14());
		fechaLecturaListada.getListaPrecios().put(15, fechaLectura.getH15());
		fechaLecturaListada.getListaPrecios().put(16, fechaLectura.getH16());
		fechaLecturaListada.getListaPrecios().put(17, fechaLectura.getH17());
		fechaLecturaListada.getListaPrecios().put(18, fechaLectura.getH18());
		fechaLecturaListada.getListaPrecios().put(19, fechaLectura.getH19());
		fechaLecturaListada.getListaPrecios().put(20, fechaLectura.getH20());
		fechaLecturaListada.getListaPrecios().put(21, fechaLectura.getH21());
		fechaLecturaListada.getListaPrecios().put(22, fechaLectura.getH22());
		fechaLecturaListada.getListaPrecios().put(23, fechaLectura.getH23());
		fechaLecturaListada.getListaPrecios().put(24, fechaLectura.getH24());
		fechaLecturaListada.getListaPrecios().put(25, fechaLectura.getH25());
		return fechaLecturaListada;
	}

	/**
	 * Recibe una fecha a consultar y devuelve los precios de omie para esa fecha en el formato del objeto
	 * @param fechaConsulta Fecha en formato yyyyMMdd
	 * @return EntityFechaLecturaOMIE
	 */
	public static EntityFechaLecturaOMIE consultarPrecio(String fechaConsulta) {
		//https://www.omie.es/es/file-download?parents%5B0%5D=marginalpdbc&filename=marginalpdbc_20200714.1 
		EntityFechaLecturaOMIE fechaLectura = null;
		try {
			// Creo la URL a la que voy a hacer la consulta
			URL url = new URL("https://www.omie.es/es/file-download?parents%5B0%5D=marginalpdbc&filename=marginalpdbc_"
					+ fechaConsulta + ".1");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
			}
			// Recojo los datos recibidos de la consulta
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);

			// Leo el mensaje linea por linea
			String output;
			while ((output = br.readLine()) != null) {
				String[] datosOMIE = output.split(";");
				// Compruebo que sea el dato a buscar
				// (los datos vienen separados por ";")
				if (datosOMIE.length > 1) {
					if (fechaLectura == null) {
						/*
						 * Le escribimos al objeto la fecha recibida e inicializamos el objeto
						 */
						int anio = Integer.parseInt(datosOMIE[0]);
						int mes = Integer.parseInt(datosOMIE[1]);
						int dia = Integer.parseInt(datosOMIE[2]);
						DateTime fecha = new DateTime(anio, mes, dia, 0, 0);
						fechaLectura = new EntityFechaLecturaOMIE();
						fechaLectura.setFecha(fecha.toDate());
					}
					// Introducimos los precios y el horario
					int horario = Integer.parseInt(datosOMIE[3]);
					double precio = Double.parseDouble(datosOMIE[5]);
					switch (horario) {
						case 1:
							fechaLectura.setH1(precio);
							break;
						case 2:
							fechaLectura.setH2(precio);
							break;
						case 3:
							fechaLectura.setH3(precio);
							break;
						case 4:
							fechaLectura.setH4(precio);
							break;
						case 5:
							fechaLectura.setH5(precio);
							break;
						case 6:
							fechaLectura.setH6(precio);
							break;
						case 7:
							fechaLectura.setH7(precio);
							break;
						case 8:
							fechaLectura.setH8(precio);
							break;
						case 9:
							fechaLectura.setH9(precio);
							break;
						case 10:
							fechaLectura.setH10(precio);
							break;
						case 11:
							fechaLectura.setH11(precio);
							break;
						case 12:
							fechaLectura.setH12(precio);
							break;
						case 13:
							fechaLectura.setH13(precio);
							break;
						case 14:
							fechaLectura.setH14(precio);
							break;
						case 15:
							fechaLectura.setH15(precio);
							break;
						case 16:
							fechaLectura.setH16(precio);
							break;
						case 17:
							fechaLectura.setH17(precio);
							break;
						case 18:
							fechaLectura.setH18(precio);
							break;
						case 19:
							fechaLectura.setH19(precio);
							break;
						case 20:
							fechaLectura.setH20(precio);
							break;
						case 21:
							fechaLectura.setH21(precio);
							break;
						case 22:
							fechaLectura.setH22(precio);
							break;
						case 23:
							fechaLectura.setH23(precio);
							break;
						case 24:
							fechaLectura.setH24(precio);
							break;
						case 25:
							fechaLectura.setH25(precio);
							break;
					}
				}
			}
			// Nos desconectamos de la consulta
			conn.disconnect();
		} catch (IOException | RuntimeException e) {
			rlogger.error("Exception in NetClientGet:- " + e);
		}
		return fechaLectura;
	}
	
	/**
	 * Busca los dias festivos de OMIE y los devuelve en forma de lista de fechas 
	 * @param anio
	 * @return
	 */
	public static LinkedList<DateTime> buscarFestivos(int anio) {
        LinkedList<DateTime> festivos = new LinkedList<DateTime>();
        String ruta = "https://www.omie.es/sites/default/files/" + anio + "-01/calendario_" + anio + ".pdf";
        try {
            BufferedInputStream in = new BufferedInputStream(new URL(ruta).openStream());
            PDDocument document = PDDocument.load(in);
            document.getClass();

            if (!document.isEncrypted()) {

                PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                stripper.setSortByPosition(true);

                PDFTextStripper tStripper = new PDFTextStripper();

                String pdfFileInText = tStripper.getText(document);
                //Lo separamos por saltos de linea
                String lines[] = pdfFileInText.split("\\r?\\n");
                //Recorro cada linea
                for (String line : lines) {
                    String[] palabra = line.split(" ");
                    if (palabra.length > 6) {
                    	//Compruebo que cada linea en la que estoy hay un registro de fecha
                        if (esNumero(palabra[0]) && esNumero(palabra[5])) {
                            int diaOMIE = Integer.parseInt(palabra[0]);
                            int mesOMIE = numeroMes(palabra[2]);
                            int anioOMIE = Integer.parseInt(palabra[5]);
                            if (mesOMIE != -1) {
                                festivos.add(new DateTime(anioOMIE, mesOMIE, diaOMIE, 0, 0));
                            } else {
                                throw new Exception("Ha ocurrido un error al "
                                        + "transformar el mes en valor numerico. "
                                        + "No existe el mes " + palabra[2]);
                            }
                        }
                    }
                }
            }
        } catch (MalformedURLException ex) {
        	rlogger.error("Error Malformacion URL: " + ex);
        } catch (IOException ex) {
        	rlogger.error("Error Abrir el archivo: " + ex);
        } catch (Exception ex) {
        	rlogger.error("Error al consultar festivos: " + ex);
        }
        return festivos;
    }
	
	/**
	 * Devuelve true si es un numero y false si no lo es 
	 * @param numero
	 * @return
	 */
	private static boolean esNumero(String numero) {
        try {
            Integer.parseInt(numero);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
	
	/**
	 * Devuelve el numero del mes pasado por string
	 * @param mes
	 * @return
	 */
	private static int numeroMes(String mes) {
        switch (mes) {
            case "enero":
                return 1;
            case "febrero":
                return 2;
            case "marzo":
                return 3;
            case "abril":
                return 4;
            case "mayo":
                return 5;
            case "junio":
                return 6;
            case "julio":
                return 7;
            case "agosto":
                return 8;
            case "septiembre":
                return 9;
            case "octubre":
                return 10;
            case "noviembre":
                return 11;
            case "diciembre":
                return 12;
            default:
                return -1;
        }
    }
}
