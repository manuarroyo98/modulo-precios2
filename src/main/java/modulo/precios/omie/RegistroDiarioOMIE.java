package modulo.precios.omie;

import java.util.Date;
import java.util.LinkedList;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import modulo.precios.omie.entities.EntityFechaLecturaOMIE;
import modulo.precios.omie.entities.EntityFestivoOMIE;
import modulo.precios.omie.services.ServiceFechaLecturaOMIE;
import modulo.precios.omie.services.ServiceFestivoOMIE;

/**
 * 
 * @author Diego Fernández Díaz
 * @version 1.0
 * @since 14/07/2020
 *
 */

@Component
@Controller
public class RegistroDiarioOMIE {
	/*
	 * Url base del controller, todos los metodos se accederan como la BASE_URL +
	 * ruta del metodo
	 */
	@Autowired
	private ServiceFestivoOMIE festivoService;
	@Autowired
	private ServiceFechaLecturaOMIE fechaLecturaService;
	
	private static final Logger rlogger = LoggerFactory.getLogger(RegistroDiarioOMIE.class);


	/**
	 * Fecha del ultimo dato Guardado
	 */
	private static DateTime fechaGuardadoDiario = null;
	private static int anioGuardadoFestivo = -1;
	/**
	 * Indica el estado de la insercion de los datos: -1: El programa acaba de
	 * iniciarse 1: El programa aun no ha registrado el dato del dia 2: El programa
	 * ya ha guardado el dato del dia
	 */
	private static int finTrabajoDiario = -1;
	private static int finTrabajoFestivo = -1;

	/**
	 * Constructor
	 * 
	 * @param festivoService
	 * @param fechaLecturaService
	 */
	public RegistroDiarioOMIE(ServiceFestivoOMIE festivoService, ServiceFechaLecturaOMIE fechaLecturaService) {
		super();
		this.festivoService = festivoService;
		this.fechaLecturaService = fechaLecturaService;
	}

	/**
	 * Registra los datos diarios en BD
	 * 
	 * Este metodo se ejecuta tantas veces como el tiempo que tenga en la variable
	 * "fixedRate" el valor es en milisegundos
	 */
	@Scheduled(fixedRate = 2 * 60 * 60 * 1000)
	public void registroDiarioDatos() {
		rlogger.info("Inicio Registro Diario");
		// Recojo la fecha actual
		DateTime fechaActual = new DateTime(new Date());
		// Compruebo si hay un historial de fechas
		if (fechaGuardadoDiario != null) {
			// Compruebo si estoy en el mismo dia que en el ultimo registro
			if (!fechaGuardadoDiario.toLocalDate().isEqual(fechaActual.toLocalDate())) {
				// si estoy en diferentes dias mando que se guarde en base de datos
				finTrabajoDiario = 1;
				rlogger.info("Cambio de dia");
			}
		}
		// Que tipo de registro realizo
		switch (finTrabajoDiario) {
		case -1:
			rlogger.info("Comprobacion de inicio de aplicacion");
			// Al ser el inicio de la aplicacion busco si ya existe un registro para esta
			// fecha
			EntityFechaLecturaOMIE fechaLectura = fechaLecturaService.buscarPorFecha(new Date());
			// si no hay un registro me devolvera Null
			if (fechaLectura == null) {
				try {
					// Consulto en la API de OMIE el registro para la fecha actual
					fechaLectura = HerramientasOMIE.consultarPrecio(fechaActual.toString("yyyyMMdd"));
					// Guardo en BD
					fechaLecturaService.save(fechaLectura);
					// Cambio el estado
					finTrabajoDiario = 2;
					rlogger.info("Datos Guardados");
				} catch (Exception e) {
					// Si no se ha registrado por algun error le pongo el estado
					finTrabajoDiario = 1;
					rlogger.error("Error al Guardar Datos: " + e);
				}
			} else {
				// Si ya estan registrados se lo indico en el estado
				rlogger.info("Datos ya estan Guardados");
				finTrabajoDiario = 2;
			}

			break;
		case 1:
			rlogger.info("Guardar nuevos datos");
			try {
				// Consulto en la API de OMIE el registro para la fecha actual
				fechaLectura = HerramientasOMIE.consultarPrecio(fechaActual.toString("yyyyMMdd"));
				// Guardo en BD
				fechaLecturaService.save(fechaLectura);
				// Cambio el estado
				finTrabajoDiario = 2;
				rlogger.info("Datos Guardados");
			} catch (Exception e) {
				// Si no se ha registrado por algun error le pongo el estado
				finTrabajoDiario = 1;
				rlogger.error("Error al Guardar Datos: " + e);
			}
			break;
		}
		// Registro la fecha actual en el historial
		fechaGuardadoDiario = fechaActual;
		rlogger.info("Fin del metodo");
	}

	/**
	 * Registra los Festivos en BD
	 * 
	 * Este metodo se ejecuta tantas veces como el tiempo que tenga en la variable
	 * "fixedRate" el valor es en milisegundos
	 */
	@Scheduled(fixedRate = 24 * 60 * 60 * 1000)
	public void registroFestivos() {
		rlogger.info("Inicio Registro Festivos");
		// Recojo la fecha actual
		DateTime fechaActual = new DateTime(new Date());
		// Si ha cambiado el año reinicio el estado
		if (anioGuardadoFestivo != -1) {
			if (anioGuardadoFestivo != fechaActual.getYear()) {
				rlogger.info("Cambio de año");
				finTrabajoFestivo = 1;
			}
		}
		// Solo voy a mandar la peticion a partir del 15/12 hasta final de mes
		if (fechaActual.getMonthOfYear() == 12 && fechaActual.getDayOfMonth() > 15) {
			// Que tipo de registro realizo
			switch (finTrabajoFestivo) {
			case -1:
				rlogger.info("Comprobacion de inicio de aplicacion");
				// Al ser el inicio de la aplicacion busco si ya existe un registro para este
				// año
				int diasFestivos = festivoService.buscarPorAnio(fechaActual.getYear()).size();
				// si no hay un registro me devolvera 0
				if (diasFestivos == 0) {
					try {
						// Consulto en la API de OMIE el registro para la fecha actual
						LinkedList<DateTime> fechaLectura = HerramientasOMIE
								.buscarFestivos(fechaActual.plusYears(1).getYear());
						// Guardo en BD
						for (DateTime dateTime : fechaLectura) {
							EntityFestivoOMIE festivo = new EntityFestivoOMIE();
							festivo.setFecha(dateTime.toDate());
							festivoService.save(festivo);
						}
						// Cambio el estado
						finTrabajoFestivo = 2;
						rlogger.info("Datos Guardados");
					} catch (Exception e) {
						// Si no se ha registrado por algun error le pongo el estado
						finTrabajoFestivo = 1;
						rlogger.error("Error al Guardar Datos: " + e);
					}
				} else {
					// Si ya estan registrados se lo indico en el estado
					rlogger.info("Datos ya estan Guardados");
					finTrabajoFestivo = 2;
				}

				break;
			case 1:
				rlogger.info("Guardar nuevos datos");
				try {
					// Consulto en la API de OMIE el registro para el año actual
					LinkedList<DateTime> fechaLectura = HerramientasOMIE.buscarFestivos(fechaActual.plusYears(1).getYear());
					// Guardo en BD
					for (DateTime dateTime : fechaLectura) {
						EntityFestivoOMIE festivo = new EntityFestivoOMIE();
						festivo.setFecha(dateTime.toDate());
						festivoService.save(festivo);
					}
					// Cambio el estado
					finTrabajoFestivo = 2;
					rlogger.info("Datos Guardados");
				} catch (Exception e) {
					// Si no se ha registrado por algun error le pongo el estado
					finTrabajoFestivo = 1;
					rlogger.error("Error al Guardar Datos: " + e);
				}
				break;
			}

		}
		// Registro la fecha actual en el historial
		anioGuardadoFestivo = fechaActual.getYear();
		rlogger.info("Fin del metodo");
	}
}
