package modulo.precios.omie;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import modulo.precios.omie.entities.EntityFechaLecturaOMIE;
import modulo.precios.omie.entities.EntityFestivoOMIE;
import modulo.precios.omie.services.ServiceFechaLecturaOMIE;
import modulo.precios.omie.services.ServiceFestivoOMIE;

import org.springframework.http.HttpStatus;

/**
 * 
 * @author Diego Fernández Díaz
 * @version 1.0
 * @since 13/07/2020
 *
 */

@RestController
@RequestMapping(RESTControllerOMIE.BASE_URL)
public class RESTControllerOMIE {

	/*
	 * Url base del controller, todos los metodos se accederan como la BASE_URL +
	 * ruta del metodo
	 */
	public static final String BASE_URL = "/moduloOMIE/api/v1/";
	
	
	private static final Logger rlogger = LoggerFactory.getLogger(RESTControllerOMIE.class);

	
	@Autowired
	private ServiceFestivoOMIE festivoOMIEService;
	@Autowired
	private ServiceFechaLecturaOMIE fechaLecturaOMIEService;

	public RESTControllerOMIE(ServiceFestivoOMIE festivoOMIEService, ServiceFechaLecturaOMIE fechaLecturaOMIEService) {
		super();
		this.festivoOMIEService = festivoOMIEService;
		this.fechaLecturaOMIEService = fechaLecturaOMIEService;
	}

	@CrossOrigin
	@GetMapping(path = "lectura/vector", produces = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public EntityFechaLecturaOMIE getEntityFechaLecturaOMIE(HttpServletRequest request, @RequestParam String strFecha) {
		Date fecha;
		try {
			// Convierto la fecha de formato String a formato sql.Date
			fecha = new SimpleDateFormat("yyyy-MM-dd").parse(strFecha);
			// Realizo la consulta y devuelvo el objeto
			EntityFechaLecturaOMIE EntityFechaLecturaOMIE = fechaLecturaOMIEService.buscarPorFecha(fecha);
			return EntityFechaLecturaOMIE;
		} catch (ParseException e) {
			// Si la fecha no tiene un formato correcto, pinto el error y devuelvo un bad
			// request
			rlogger.error("Error al formatear la fecha. \n\tFecha URL: " + strFecha);
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Formato de fecha es incorrecto", e);
		}catch (NullPointerException e) {
			return new EntityFechaLecturaOMIE();
		} catch (Exception e) {
			rlogger.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error desconocido", e);
		}
	}
	
	@CrossOrigin
	@GetMapping(path = "lecturas/vector", produces = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public List<EntityFechaLecturaOMIE> getEntityFechaLecturaOMIEEntreFechas(HttpServletRequest request, @RequestParam String strFechaInicio,
			@RequestParam String strFechaFin) {
		DateTime fechaInicio, fechaFin;
		try {
			// Convierto la fecha de formato String a formato sql.Date
			fechaInicio = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(strFechaInicio);
			fechaFin = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(strFechaFin).plusDays(1);
			// Realizo la consulta y devuelvo el objeto
			List<EntityFechaLecturaOMIE> listaEntityFechaLecturaOMIE = fechaLecturaOMIEService.buscarEntreFechas(fechaInicio.toDate(), fechaFin.toDate());
			return listaEntityFechaLecturaOMIE;
		} catch (IllegalFieldValueException e) {
			// Si la fecha no tiene un formato correcto, pinto el error y devuelvo un bad
			// request
			rlogger.error("Error al formatear las fechas. \n\tFecha Inicio: " + strFechaInicio + " \n\tFecha Fin: "
					+ strFechaFin);
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Formato de fecha es incorrecto", e);
		} catch (NullPointerException e) {
			return new ArrayList<EntityFechaLecturaOMIE>();
		} catch (Exception e) {
			rlogger.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error desconocido", e);
		}
	}

	@CrossOrigin
	@GetMapping(path = "lectura/columna", produces = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public ObjFechaLecturaListada getEntityFechaLecturaOMIEListada(HttpServletRequest request, @RequestParam String strFecha) {
		Date fecha;
		try {
			// Convierto la fecha de formato String a formato sql.Date
			fecha = new SimpleDateFormat("yyyy-MM-dd").parse(strFecha);
			// Realizo la consulta y devuelvo el objeto con formato de columnas
			EntityFechaLecturaOMIE EntityFechaLecturaOMIE = fechaLecturaOMIEService.buscarPorFecha(fecha);
			return HerramientasOMIE.formatearFechaLectura(EntityFechaLecturaOMIE);
		} catch (ParseException e) {
			// Si la fecha no tiene un formato correcto, pinto el error y devuelvo un bad
			// request
			rlogger.error("Error al formatear la fecha. \n\tFecha URL: " + strFecha);
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Formato de fecha es incorrecto", e);
		}catch (NullPointerException e) {
			return new ObjFechaLecturaListada();
		} catch (Exception e) {
			rlogger.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error desconocido", e);
		}
	}

	@CrossOrigin
	@GetMapping(path = "lecturas/columna", produces = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public List<ObjFechaLecturaListada> getEntityFechaLecturaOMIEListadaEntreFechas(HttpServletRequest request,
			@RequestParam String strFechaInicio, @RequestParam String strFechaFin) {
		DateTime fechaInicio, fechaFin;
		List<ObjFechaLecturaListada> listaEntityFechaLecturaOMIEListadas = new ArrayList<ObjFechaLecturaListada>();
		try {
			// Convierto la fecha de formato String a formato sql.Date
			fechaInicio = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(strFechaInicio);
			fechaFin = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(strFechaFin).plusDays(1);
			// Realizo la consulta
			List<EntityFechaLecturaOMIE> listaEntityFechaLecturaOMIE = fechaLecturaOMIEService.buscarEntreFechas(fechaInicio.toDate(), fechaFin.toDate());
			// Genero el formato en columnas
			for (EntityFechaLecturaOMIE EntityFechaLecturaOMIE : listaEntityFechaLecturaOMIE) {
				listaEntityFechaLecturaOMIEListadas.add(HerramientasOMIE.formatearFechaLectura(EntityFechaLecturaOMIE));
			}
			// Devuelvo el objeto
			return listaEntityFechaLecturaOMIEListadas;
		} catch (IllegalFieldValueException e) {
			// Si la fecha no tiene un formato correcto, pinto el error y devuelvo un bad
			// request
			rlogger.error("Error al formatear las fechas. \n\tFecha Inicio: " + strFechaInicio + " \n\tFecha Fin: "
					+ strFechaFin);
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Formato de fecha es incorrecto", e);
		}catch (NullPointerException e) {
			return new ArrayList<ObjFechaLecturaListada>();
		} catch (Exception e) {
			rlogger.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error desconocido", e);
		}
	}
	
	@CrossOrigin
	@GetMapping(path = "lecturas/rangoFechas", produces = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public List<String> getFechasPerdidas(HttpServletRequest request) {
		List<String> arrayFechas = new ArrayList<String>();
		arrayFechas.add(fechaLecturaOMIEService.fechaPrimerRegistro());
		arrayFechas.add(fechaLecturaOMIEService.fechaUltimoRegistro());
		return arrayFechas;
	}

	@CrossOrigin
	@GetMapping(path = "EntityFestivoOMIEs", produces = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	List<EntityFestivoOMIE> getEntityFestivoOMIEs(HttpServletRequest request, @RequestParam int anio) {
		List<EntityFestivoOMIE> listaEntityFestivoOMIE = festivoOMIEService.buscarPorAnio(anio);
		return listaEntityFestivoOMIE;
	}
	
	@CrossOrigin
	@GetMapping(path = "guardar", produces = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public EntityFechaLecturaOMIE guardar(HttpServletRequest request,
			@RequestParam String fecha) {
		// Consulto en la API de OMIE el registro para la fecha actual
		EntityFechaLecturaOMIE EntityFechaLecturaOMIE = HerramientasOMIE.consultarPrecio(fecha);
		// Guardo en BD
		//EntityFechaLecturaOMIEService.save(EntityFechaLecturaOMIE);
		
		return EntityFechaLecturaOMIE;
	}
}

