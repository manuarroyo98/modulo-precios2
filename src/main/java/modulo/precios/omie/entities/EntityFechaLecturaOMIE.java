package modulo.precios.omie.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

@Entity
@Table(name = "fecha_lectura_omie")
public class EntityFechaLecturaOMIE implements Serializable {

	@Id
	@Column(name = "id_fecha_lectura")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idFechaLectura;
	
	@CreatedDate
	@Temporal(TemporalType.DATE)
	@Column(name = "fecha", nullable = false)
	private Date fecha;
	
	@Column(name = "h1", nullable = false)
	private double h1;
	
	@Column(name = "h2", nullable = false)
	private double h2;
	
	@Column(name = "h3", nullable = false)
	private double h3;
	
	@Column(name = "h4", nullable = false)
	private double h4;
	
	@Column(name = "h5", nullable = false)
	private double h5;
	
	@Column(name = "h6", nullable = false)
	private double h6;
	
	@Column(name = "h7", nullable = false)
	private double h7;
	
	@Column(name = "h8", nullable = false)
	private double h8;
	
	@Column(name = "h9", nullable = false)
	private double h9;
	
	@Column(name = "h10", nullable = false)
	private double h10;
	
	@Column(name = "h11", nullable = false)
	private double h11;
	
	@Column(name = "h12", nullable = false)
	private double h12;
	
	@Column(name = "h13", nullable = false)
	private double h13;
	
	@Column(name = "h14", nullable = false)
	private double h14;
	
	@Column(name = "h15", nullable = false)
	private double h15;
	
	@Column(name = "h16", nullable = false)
	private double h16;
	
	@Column(name = "h17", nullable = false)
	private double h17;
	
	@Column(name = "h18", nullable = false)
	private double h18;
	
	@Column(name = "h19", nullable = false)
	private double h19;
	
	@Column(name = "h20", nullable = false)
	private double h20;
	
	@Column(name = "h21", nullable = false)
	private double h21;
	
	@Column(name = "h22", nullable = false)
	private double h22;
	
	@Column(name = "h23", nullable = false)
	private double h23;
	
	@Column(name = "h24", nullable = false)
	private double h24;
	
	@Column(name = "h25", nullable = false)
	private double h25;
	
    
	public EntityFechaLecturaOMIE(int idFechaLectura, Date fecha, double h1, double h2, double h3, double h4, double h5,
			double h6, double h7, double h8, double h9, double h10, double h11, double h12, double h13, double h14,
			double h15, double h16, double h17, double h18, double h19, double h20, double h21, double h22, double h23,
			double h24, double h25) {
		super();
		this.idFechaLectura = idFechaLectura;
		this.fecha = fecha;
		this.h1 = h1;
		this.h2 = h2;
		this.h3 = h3;
		this.h4 = h4;
		this.h5 = h5;
		this.h6 = h6;
		this.h7 = h7;
		this.h8 = h8;
		this.h9 = h9;
		this.h10 = h10;
		this.h11 = h11;
		this.h12 = h12;
		this.h13 = h13;
		this.h14 = h14;
		this.h15 = h15;
		this.h16 = h16;
		this.h17 = h17;
		this.h18 = h18;
		this.h19 = h19;
		this.h20 = h20;
		this.h21 = h21;
		this.h22 = h22;
		this.h23 = h23;
		this.h24 = h24;
		this.h25 = h25;
	}

	public EntityFechaLecturaOMIE() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	/**
	 * METODOS GETTER Y SETTER
	 */
	
	
	public int getIdFechaLectura() {
		return idFechaLectura;
	}

	public void setIdFechaLectura(int idFechaLectura) {
		this.idFechaLectura = idFechaLectura;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public double getH1() {
		return h1;
	}

	public void setH1(double h1) {
		this.h1 = h1;
	}

	public double getH2() {
		return h2;
	}

	public void setH2(double h2) {
		this.h2 = h2;
	}

	public double getH3() {
		return h3;
	}

	public void setH3(double h3) {
		this.h3 = h3;
	}

	public double getH4() {
		return h4;
	}

	public void setH4(double h4) {
		this.h4 = h4;
	}

	public double getH5() {
		return h5;
	}

	public void setH5(double h5) {
		this.h5 = h5;
	}

	public double getH6() {
		return h6;
	}

	public void setH6(double h6) {
		this.h6 = h6;
	}

	public double getH7() {
		return h7;
	}

	public void setH7(double h7) {
		this.h7 = h7;
	}

	public double getH8() {
		return h8;
	}

	public void setH8(double h8) {
		this.h8 = h8;
	}

	public double getH9() {
		return h9;
	}

	public void setH9(double h9) {
		this.h9 = h9;
	}

	public double getH10() {
		return h10;
	}

	public void setH10(double h10) {
		this.h10 = h10;
	}

	public double getH11() {
		return h11;
	}

	public void setH11(double h11) {
		this.h11 = h11;
	}

	public double getH12() {
		return h12;
	}

	public void setH12(double h12) {
		this.h12 = h12;
	}

	public double getH13() {
		return h13;
	}

	public void setH13(double h13) {
		this.h13 = h13;
	}

	public double getH14() {
		return h14;
	}

	public void setH14(double h14) {
		this.h14 = h14;
	}

	public double getH15() {
		return h15;
	}

	public void setH15(double h15) {
		this.h15 = h15;
	}

	public double getH16() {
		return h16;
	}

	public void setH16(double h16) {
		this.h16 = h16;
	}

	public double getH17() {
		return h17;
	}

	public void setH17(double h17) {
		this.h17 = h17;
	}

	public double getH18() {
		return h18;
	}

	public void setH18(double h18) {
		this.h18 = h18;
	}

	public double getH19() {
		return h19;
	}

	public void setH19(double h19) {
		this.h19 = h19;
	}

	public double getH20() {
		return h20;
	}

	public void setH20(double h20) {
		this.h20 = h20;
	}

	public double getH21() {
		return h21;
	}

	public void setH21(double h21) {
		this.h21 = h21;
	}

	public double getH22() {
		return h22;
	}

	public void setH22(double h22) {
		this.h22 = h22;
	}

	public double getH23() {
		return h23;
	}

	public void setH23(double h23) {
		this.h23 = h23;
	}

	public double getH24() {
		return h24;
	}

	public void setH24(double h24) {
		this.h24 = h24;
	}

	public double getH25() {
		return h25;
	}

	public void setH25(double h25) {
		this.h25 = h25;
	}
}
