package modulo.precios.omie.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

@Entity
@Table(name = "festivo_omie")
public class EntityFestivoOMIE implements Serializable{

	@Id
	@Column(name = "id_festivo")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idFestivo;
	
	@CreatedDate
	@Temporal(TemporalType.DATE)
	@Column(name = "fecha", nullable = false)
	private Date fecha;
	
	public EntityFestivoOMIE(int idFestivo, Date fecha) {
		super();
		this.idFestivo = idFestivo;
		this.fecha = fecha;
	}

	public EntityFestivoOMIE() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getIdFestivo() {
		return idFestivo;
	}

	public void setIdFestivo(int idFestivo) {
		this.idFestivo = idFestivo;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
}
