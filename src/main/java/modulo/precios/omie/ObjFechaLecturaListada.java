package modulo.precios.omie;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ObjFechaLecturaListada {

	private Date fecha;
	private Map<Integer, Double> listaPrecios;
	
	public ObjFechaLecturaListada() {
		super();
		this.listaPrecios = new HashMap<Integer, Double>();
	}

	public ObjFechaLecturaListada(Date fecha, Map<Integer, Double> listaPrecios) {
		super();
		this.fecha = fecha;
		this.listaPrecios = listaPrecios;
	}
	
	/**
	 * GETTER Y SETTER
	 * 
	 */

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Map<Integer, Double> getListaPrecios() {
		return listaPrecios;
	}

	public void setListaPrecios(Map<Integer, Double> listaPrecios) {
		this.listaPrecios = listaPrecios;
	}
}
